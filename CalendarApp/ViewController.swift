//
//  ViewController.swift
//  CalendarApp
//
//  Created by Subham Khandelwal on 25/02/18.
//  Copyright © 2018 calendar. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {
    
    let reuseId : String = "calendarCell"
    let tableReuseId : String = "agendaCell"
    var startDate : Date?
    var endDate : Date?
    let formatter = DateFormatter()
    let screenSize = UIScreen.main.bounds
    
    // I have added few events for every month in memory. In real life scenario, this would come from server or would be stored locally somewhere
    let eventsDetails = ["03" : "Today is 3rd day of Month.",
                         "06" : "Today is 6th day of Month.",
                         "10" : "Today is 10th day of Month.",
                         "12" : "Today is 12th day of Month.",
                         "19" : "Today is 19th day of Month.",
                         "22" : "Today is 22th day of Month.",
                         "27" : "Today is 27th day of Month."]
    
    @IBOutlet weak var calendarCollectionView: CLCalendarCollectionView!
    @IBOutlet weak var agendaTableView: UITableView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var daysView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureView()
        
        // I have started from Jan 2006 to Dec 2026 (20 Years)
        formatter.dateFormat = "yyyy/MM/dd"
        startDate = formatter.date(from: "2006/01/01")!
        endDate = formatter.date(from: "2026/12/31")!
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let daysPassed = CLHelperClass.calculateDaysBetweenTwoDates(start: startDate!, end: Date())
        calendarCollectionView.scrollToItem(at: IndexPath(item: daysPassed-1, section: 0), at: .top, animated: false)
        agendaTableView.scrollToRow(at: IndexPath(row: 0, section: daysPassed-1), at: .top, animated: false)
    }
    
    func configureView() {
        calendarCollectionView.register(UINib(nibName: "CLCalendarCollectionCell", bundle: nil), forCellWithReuseIdentifier: reuseId)
        agendaTableView.register(UITableViewCell.self, forCellReuseIdentifier: tableReuseId)
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if (scrollView == agendaTableView) {
            collectionViewHeight.constant = -150
        } else {
            collectionViewHeight.constant = 0
        }
    }
    
    // MARK: CollectionView Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return CLHelperClass.calculateDaysBetweenTwoDates(start: startDate!, end: endDate!)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : CLCalendarCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath) as! CLCalendarCollectionCell
        let currentDate = Calendar.current.date(byAdding: .day, value: indexPath.row, to: startDate!)!
        formatter.dateFormat = "dd"
        let day = formatter.string(from: currentDate)
        formatter.dateFormat = "MMM"
        let month = formatter.string(from: currentDate)
        formatter.dateFormat = "MM"
        let monthNumber = formatter.string(from: currentDate)
        let isOddMonth = Int(monthNumber)! % 2 == 1
        let doesEventExist = eventsDetails[day] != nil
        cell.configureView(day, doesEventExist, day == "01" ? month : "", isOddMonth)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: screenSize.width/7-3, height: screenSize.width/7-3)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // On clicking collection view scroll the agenda table view
        agendaTableView.scrollToRow(at: IndexPath(row: 0, section: indexPath.row), at: .top, animated: true)
        // Reset height
        collectionViewHeight.constant = 0
    }
    
    
    // MARK: TableView Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return CLHelperClass.calculateDaysBetweenTwoDates(start: startDate!, end: endDate!)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 20))
        headerView.backgroundColor = UIColor.gray
        let currentDate = Calendar.current.date(byAdding: .day, value: section, to: startDate!)!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, dd MMM YYYY"
        let day = dateFormatter.string(from: currentDate)
        headerView.text = day
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableReuseId)
        let currentDate = Calendar.current.date(byAdding: .day, value: indexPath.section, to: startDate!)!
        formatter.dateFormat = "dd"
        let day = formatter.string(from: currentDate)
        cell?.textLabel?.text = eventsDetails[day] != nil ? eventsDetails[day] : "No Events"
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let alertController = UIAlertController(title: "", message: "Show details screen as per design", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: false, completion: nil)
    }
}
