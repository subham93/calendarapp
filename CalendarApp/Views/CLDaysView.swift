//
//  CLDaysView.swift
//  CalendarApp
//
//  Created by Subham Khandelwal on 26/02/18.
//  Copyright © 2018 calendar. All rights reserved.
//

import Foundation
import UIKit

class CLDaysView : UIView {
    
    let days = ["S", "M", "T", "W", "T", "F", "S"]
    override func awakeFromNib() {
        super.awakeFromNib()
        self.configureUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configureUI() {
        var lastX : CGFloat = 0.0
        for i in 0...6 {
            let daysLbl = UILabel(frame: CGRect(x: lastX, y: 0, width: UIScreen.main.bounds.width/7, height: 25))
            daysLbl.text = days[i]
            daysLbl.textAlignment = .center
            self.addSubview(daysLbl)
            lastX += UIScreen.main.bounds.width/7
        }
    }
}
