//
//  CLCalendarCollectionCell.swift
//  CalendarApp
//
//  Created by Subham Khandelwal on 25/02/18.
//  Copyright © 2018 calendar. All rights reserved.
//

import Foundation
import UIKit

class CLCalendarCollectionCell : UICollectionViewCell {
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var agendaLbl: UILabel!
    @IBOutlet weak var monthLbl: UILabel!

    func configureView(_ date : String, _ isAgendaPresent : Bool, _ month : String, _ isOddMonth : Bool) {
        dateLbl.text = date
        agendaLbl.isHidden = !isAgendaPresent
        monthLbl.text = month
        if (isOddMonth) {
            self.backgroundColor = UIColor.white
        } else {
            self.backgroundColor = UIColor.yellow
        }
    }
}
