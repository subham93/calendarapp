//
//  CLHelperClass.swift
//  CalendarApp
//
//  Created by Subham Khandelwal on 25/02/18.
//  Copyright © 2018 calendar. All rights reserved.
//

import Foundation

class CLHelperClass {
    
    static func calculateDaysBetweenTwoDates(start: Date, end: Date) -> Int {
        
        let currentCalendar = Calendar.current
        guard let start = currentCalendar.ordinality(of: .day, in: .era, for: start) else {
            return 0
        }
        guard let end = currentCalendar.ordinality(of: .day, in: .era, for: end) else {
            return 0
        }
        return end - start
    }
}
